
//preloader
$(window).load(function(){

$(".preloader").delay(1000).fadeOut("slow") 

});

// Load is used to ensure all images have been loaded, impossible with document



//navbar
$(function() {
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

// Highlight the top nav as scrolling occurs
$('body').scrollspy({
    target: '.navbar-fixed-top'
})

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});



// transition
$(function() {
$(".from-left").smoove({
    moveX   : '-100px',
  });
$(".from-right").smoove({
    moveX   : '100px',
  });
$(".from-bottom").smoove({
    moveY   : '100px',
  });
$(".from-top").smoove({
    moveY   : '-100px',
  });
$(".move3d").smoove({
    scale3d   : '2,3,0.5',
  });
    
});


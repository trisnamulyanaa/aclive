$(document).ready(function(){

$(".setting-btn").click(function () {
    $(".settings").toggleClass("show");
});
$("#default").click(function () {
$("#skin").attr("href", "css/theme-default.css");
});
$("#green").click(function () {
    $("#skin").attr("href", "css/theme-green.css");
});
$("#light-blue").click(function () {
    $("#skin").attr("href", "css/theme-light-blue.css");
});

$("#orange").click(function () {
    $("#skin").attr("href", "css/theme-orange.css");
});
    
$("#red").click(function () {
$("#skin").attr("href", "css/theme-red.css");
});
    
$("#yellow").click(function () {
$("#skin").attr("href", "css/theme-yellow.css");
});
 $(function(){
      // bind change event to select
      $('#dynamic_select').bind('change', function () {
          var url = $(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });
    });

});